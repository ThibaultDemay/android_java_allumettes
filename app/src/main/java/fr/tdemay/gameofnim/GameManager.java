package fr.tdemay.gameofnim;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import static java.lang.Math.random;

public class GameManager implements Parcelable {
    private Player movingPlayer; // le joueur qui a le trait
    private int maxSticks = 3; // le nombre maximum d'allumettes qu'un joueur peut tirer en un coup
    private int nbInitSticks = 20; // le nombre d'allumettes au départ
    private int nbRemainingSticks; // le nombre d'allumettes à l'instant t
    private Player player1;
    private Player player2;
    private Player winnerPlayer;
    private ArrayList<Integer> moveList;
    private boolean stillRunning; // un boolean renseignant l'état du jeu

    public GameManager(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.movingPlayer = player1; // player1 joue en premier
        this.nbRemainingSticks = this.nbInitSticks; // on met les allumettes restantes à la valeur init
        this.winnerPlayer = null;
        this.stillRunning = true;
        this.moveList = new ArrayList<>();
    }

    // la fonction permettant à un joueur de retirer des sticks

    public void move(Player player, int nbTakenSticks) {
        try {
            if (!this.stillRunning) {
                throw new Exception("La partie est finie");
            } else {
                if ((nbTakenSticks > this.maxSticks) || (nbTakenSticks < 1)) {
                    throw new Exception("Le nombre d'allumettes à retirer dépasse le maximum autorisé");
                } else {
                    if (player != this.movingPlayer) {
                        throw new Exception("Ce joueur a joué le dernier coup");
                    } else {
                        this.validMove(nbTakenSticks); // on lance un coup valide
                    }
                }
            }
        } catch (Exception e) {
            //System.err.println("ERROR : Les conditions de la fct MOVE ne sont pas respectées !");
        }

    }

    private void validMove(int nbTakenSticks) {

        nbRemainingSticks -= nbTakenSticks; // on retire les n sticks
        this.moveList.add(nbTakenSticks); // on ajoute le nouveau coup à la liste des coups joués
        // on change de joueur si la partie n'est pas finie
        if (this.movingPlayer == player1) {
            this.movingPlayer = player2;
        } else {
            this.movingPlayer = player1;
        }
        System.err.println("Allumettes restantes : " + nbRemainingSticks);
        if (nbRemainingSticks <= 0) {
            System.err.println("ENDGAME : La partie est finie");
            this.end();
        }
    }

    // la fonction de fin de jeu
    public void end() {
        this.setStillRunning(false);
        this.winnerPlayer = movingPlayer; // le jeu vient de s'arrêter, et la fct move à changer le prochain joueur : c'est donc le gagnant qui y est stocké actuellement
        this.winnerPlayer.setScore(winnerPlayer.getScore() + 1);
    }

    // une fonction qui donne un entier pseudo random entre 1 et le nombre max d'allumettes tirées en un coup
    public int rdmNumber() {
        return (int) (random() * (maxSticks)) + 1;
    }

    // les getters et setters
    public void setMovingPlayer(Player movingPlayer) {
        this.movingPlayer = movingPlayer;
    }

    public int getNbInitSticks() {
        return nbInitSticks;
    }

    public void setNbInitSticks(int nbInitSticks) {
        this.nbInitSticks = nbInitSticks;
    }

    public int getNbRemainingSticks() {
        return nbRemainingSticks;
    }

    public void setNbRemainingSticks(int nbRemainingSticks) {
        this.nbRemainingSticks = nbRemainingSticks;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public Player getMovingPlayer() {
        return movingPlayer;
    }

    public int getMaxSticks() {
        return maxSticks;
    }

    public void setMaxSticks(int maxSticks) {
        this.maxSticks = maxSticks;
    }

    public Player getWinnerPlayer() {
        return winnerPlayer;
    }

    public void setWinnerPlayer(Player winnerPlayer) {
        this.winnerPlayer = winnerPlayer;
    }

    public ArrayList<Integer> getMoveList() {
        return moveList;
    }

    public void setMoveList(ArrayList<Integer> moveList) {
        this.moveList = moveList;
    }

    public boolean isStillRunning() {
        return stillRunning;
    }

    public void setStillRunning(boolean stillRunning) {
        this.stillRunning = stillRunning;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(age);
        out.writeString(name);
        out.writeList(nicknames);
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };


}

package fr.tdemay.gameofnim;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    private TextView ui_startmenu_pseudoP1Input;
    private TextView ui_startmenu_pseudoP2Input;
    private TextView ui_startmenu_infosLabel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startmenu);
        ui_startmenu_pseudoP1Input = findViewById(R.id.pseudoP1Input);
        ui_startmenu_pseudoP2Input = findViewById(R.id.pseudoP2Input);
        Button ui_startmenu_startButton = findViewById(R.id.startButton);
        ui_startmenu_infosLabel = findViewById(R.id.infosMenuLabel);
    }

    public void submitStartButtonClick(View submitButton) {
        String pseudo1 = ui_startmenu_pseudoP1Input.getText().toString();
        String pseudo2 = ui_startmenu_pseudoP2Input.getText().toString();
        if (pseudo1.length() > 3 && pseudo2.length() > 3) {
            Player player1 = new Player(pseudo1);
            Player player2 = new Player(pseudo2);
            Intent gameIntent = new Intent(MainActivity.this, GameActivity.class);
            gameIntent.putExtra("player1", player1);
            startActivity(new Intent);
        } else {
            ui_startmenu_infosLabel.setText(R.string.error_messages_pseudos4carac);

        }
    }


}

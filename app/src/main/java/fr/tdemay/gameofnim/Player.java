package fr.tdemay.gameofnim;

import android.os.Parcel;
import android.os.Parcelable;

public class Player implements Parcelable {

    private String pseudo;
    private int score;


    public Player(String pseudo) {
        this.pseudo = pseudo;
        this.score = 0;
    }

    public String getPseudo() {
        return pseudo;
    }

    public boolean setPseudo(String pseudo) {
        boolean isOk = false;
        try {
            if (pseudo.length() > 3) {
                this.pseudo = pseudo;
                isOk = true;
            } else {
                throw new Exception("Ce pseudo n'est pas valide");
            }
        } catch (Exception e) {
        } finally {
            return isOk;
        }
    }


    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(score);
        out.writeString(pseudo);
    }

    public static final Parcelable.Creator<Player> CREATOR = new Parcelable.Creator<Player>() {
        public Player createFromParcel(Parcel in) {
            return new Player(in);
        }

        public Player[] newArray(int size) {
            return new Player[size];
        }
    };

    private Player(Parcel in) {
        this.score = in.readInt();
        this.pseudo = in.readString();
    }
}


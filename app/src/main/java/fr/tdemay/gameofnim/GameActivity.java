package fr.tdemay.gameofnim;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class GameActivity extends AppCompatActivity {

    private TextView ui_game_player1pseudoLabel;
    private TextView ui_game_player2pseudoLabel;
    private TextView ui_game_player1scoreLabel;
    private TextView ui_game_player2scoreLabel;
    private TextView ui_game_movingPlayerLabel;
    private TextView ui_game_remainingSticks;
    private SeekBar ui_game_progressbar;
    public GameManager gameManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        GameManager gameManager = new GameManager(player1, player2);
        ui_game_movingPlayerLabel = (TextView) findViewById(R.id.movingPlayerLabel);
        ui_game_movingPlayerLabel.setText(gameManager.getMovingPlayer().getPseudo().toString());
        ui_game_player1pseudoLabel = (TextView) findViewById(R.id.player1PseudoLabel);
        ui_game_player1pseudoLabel.setText(R.string.game_joueur + " : " + String.valueOf(gameManager.getPlayer1().getPseudo()));
        ui_game_player2pseudoLabel = (TextView) findViewById(R.id.player2PseudoLabel);
        ui_game_player2pseudoLabel.setText(R.string.game_joueur + " : " + String.valueOf(gameManager.getPlayer2().getPseudo()));
        ui_game_player1scoreLabel = (TextView) findViewById(R.id.player1ScoreLabel);
        ui_game_player1scoreLabel.setText(String.valueOf(gameManager.getPlayer1().getScore()));
        ui_game_player2scoreLabel = (TextView) findViewById(R.id.player2ScoreLabel);
        ui_game_player2scoreLabel.setText(String.valueOf(gameManager.getPlayer2().getScore()));
        ui_game_remainingSticks = (TextView) findViewById(R.id.remainingSticksLabel);
        ui_game_remainingSticks.setText(String.valueOf(gameManager.getNbRemainingSticks()));

        ui_game_progressbar = (SeekBar) findViewById(R.id.progressBar);


    }

    public void submitMoveButtonClick(View submitButton) {
        int moveSticks = ui_game_progressbar.getProgress();
        gameManager.move(gameManager.getMovingPlayer(), moveSticks);
    }
}

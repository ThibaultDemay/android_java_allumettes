package fr.tdemay.gameofnim;

import org.junit.Test;

import static org.junit.Assert.*;

public class GameManagerTest {

    @Test
    public void startingTest() {
        Player player1 = new Player("Newton");
        Player player2 = new Player("Aristote");
        GameManager gameManager = new GameManager(player1, player2);
        //on teste que l'on a bien le bon nombre d'allumettes en jeu, ici 20
        assertEquals(20, gameManager.getNbRemainingSticks());
        // on teste que le premier joueur est bien le joueur1
        assertEquals(player1, gameManager.getMovingPlayer());

        assertEquals(0, player1.getScore()); // on vérifie les bons scores initiaux
        assertEquals(0, player2.getScore());
    }

    @Test
    public void moveTest() {
        Player player1 = new Player("Newton");
        Player player2 = new Player("Aristote");
        GameManager gameManager = new GameManager(player1, player2);
        int currentRemainingSticks = gameManager.getNbRemainingSticks(); // on stock le nobmre restant avant tentative de move

        //Un test pour vérifier la prise en compte d'un coup
        int sticks = gameManager.rdmNumber();
        gameManager.move(player1, sticks);
        assertEquals(gameManager.getNbInitSticks() - sticks, gameManager.getNbRemainingSticks());

        // un test pour vérifier que l'initiative est bien passée au joueur 2
        assertEquals(player2, gameManager.getMovingPlayer());

        // un test pour valider l'impossibilité de jouer deux fois d'affilé
        sticks = gameManager.rdmNumber();
        currentRemainingSticks = gameManager.getNbRemainingSticks(); // on stock le nobmre restant avant tentative de move
        gameManager.move(player1, sticks); // va renvoyer une erreur
        assertEquals(currentRemainingSticks, gameManager.getNbRemainingSticks());

        // un test pour valider deux joueurs qui jouent alternativement
        currentRemainingSticks = gameManager.getNbRemainingSticks(); // on stock le nobmre restant avant tentative de move
        gameManager.move(player2, sticks);
        assertEquals(currentRemainingSticks - sticks, gameManager.getNbRemainingSticks());

        // un test pour vérifier qu'on ne peut pas retirer plus d'allumettes qu'autorisé
        currentRemainingSticks = gameManager.getNbRemainingSticks();
        gameManager.move(player1, gameManager.getMaxSticks() + 1); // va renvoyer une erreur
        gameManager.move(player1, 0); // va renvoyer une erreur
        assertEquals(currentRemainingSticks, gameManager.getNbRemainingSticks());


    }

    @Test
    public void endTest() {
        Player player1 = new Player("Newton");
        Player player2 = new Player("Aristote");
        GameManager gameManager = new GameManager(player1, player2);
        assertEquals(null, gameManager.getWinnerPlayer()); // on check que l'on a pas de vainqueur

        //une séquence de coup menant joueur2 à la victoire !
        gameManager.move(player1, 3); //17
        gameManager.move(player2, 3);// 14
        gameManager.move(player1, 3);//11
        gameManager.move(player2, 3);//8
        gameManager.move(player1, 3);//5
        gameManager.move(player2, 3);//2
        gameManager.move(player1, 3); //-1

        assertEquals(false, gameManager.isStillRunning()); // on check l'état de la partie
        assertEquals(-1, gameManager.getNbRemainingSticks()); // on check la valeur finale de la partie -1
        gameManager.move(player2, 3); //va lever une erreur
        assertEquals(-1, gameManager.getNbRemainingSticks()); // on check qu'on ne peut pas jouer davantage
        assertEquals(player2, gameManager.getWinnerPlayer()); // on check que joueur 2 est bien vainqueur
        assertEquals(1, gameManager.getWinnerPlayer().getScore());


    }

    @Test
    public void restartTest() {
        Player player1 = new Player("Newton");
        Player player2 = new Player("Aristote");
        GameManager gameManager = new GameManager(player1, player2);
        assertEquals(null, gameManager.getWinnerPlayer()); // on check que l'on a pas de vainqueur

        //une séquence de coup menant joueur2 à la victoire !
        gameManager.move(player1, 3); //17
        gameManager.move(player2, 3);// 14
        gameManager.move(player1, 3);//11
        gameManager.move(player2, 3);//8
        gameManager.move(player1, 3);//5
        gameManager.move(player2, 3);//2
        gameManager.move(player1, 3); //-1

        gameManager = new GameManager(player1, player2);
        assertEquals(null, gameManager.getWinnerPlayer()); // on check que l'on a pas de vainqueur
        assertEquals(0, gameManager.getPlayer1().getScore());
        assertEquals(1, gameManager.getPlayer2().getScore());
    }

}
